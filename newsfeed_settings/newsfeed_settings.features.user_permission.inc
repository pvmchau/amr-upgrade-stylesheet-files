<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function newsfeed_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer vc newsfeed
  $permissions['administer vc newsfeed'] = array(
    'name' => 'administer vc newsfeed',
    'roles' => array(
      '0' => 'administer',
    ),
  );

  // Exported permission: view vc newsfeed
  $permissions['view vc newsfeed'] = array(
    'name' => 'view vc newsfeed',
    'roles' => array(
      '0' => 'administer',
      '1' => 'authenticated user',
    ),
  );

  return $permissions;
}
