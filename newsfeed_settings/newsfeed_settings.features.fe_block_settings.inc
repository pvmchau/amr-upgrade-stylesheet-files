<?php

/**
 * Implementation of hook_default_fe_block_settings().
 */
function newsfeed_settings_default_fe_block_settings() {
  $export = array();

  // academicroom
  $theme = array();

  $theme['block-my_disciplines'] = array(
    'module' => 'block',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-134',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed
newsfeed/*
mynewsfeed',
    'title' => 'My Disciplines',
    'cache' => '-1',
    'machine_name' => 'my_disciplines',
  );

  $theme['block-my_room'] = array(
    'module' => 'block',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-136',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed
mynewsfeed',
    'title' => 'My Room',
    'cache' => '-1',
    'machine_name' => 'my_room',
  );

  $theme['views-news_feed_blocks-block_1'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_1',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-129',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed
newsfeed/*
mynewsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_2'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_2',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-127',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed
newsfeed/*
mynewsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_3'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_3',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-131',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed
newsfeed/*
mynewsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $export['academicroom'] = $theme;

  $theme_default = variable_get('theme_default', 'garland');
  $themes = list_themes();
  foreach ($export as $theme_key => $settings) {
    if ($theme_key != $theme_default && empty($themes[$theme_key]->status)) {
      unset($export[$theme_key]);
    }
  }
  return $export;
}
