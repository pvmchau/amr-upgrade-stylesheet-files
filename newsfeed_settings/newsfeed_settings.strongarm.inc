<?php

/**
 * Implementation of hook_strongarm().
 */
function newsfeed_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'newsfeed_content_type';
  $strongarm->value = array(
    'article' => 'article',
    'blog' => 'blog',
    'blog_post' => 'blog_post',
    'aca_book' => 'aca_book',
    'book_chapter' => 'book_chapter',
    'book_review' => 'book_review',
    'course_lecture' => 'course_lecture',
    'discussions' => 'discussions',
    'groups' => 'groups',
    'images' => 'images',
    'syllabus' => 'syllabus',
    'video' => 'video',
    'achievements' => 0,
    'audio' => 0,
    'bibliographies' => 0,
    'conference_paper' => 0,
    'contact' => 0,
    'date' => 0,
    'dissertation' => 0,
    'education' => 0,
    'education_fields_group' => 0,
    'highschool' => 0,
    'honors_awards' => 0,
    'image' => 0,
    'journal' => 0,
    'manuscripts' => 0,
    'news' => 0,
    'page' => 0,
    'profession_mem' => 0,
    'profile_achievement' => 0,
    'publications_summary' => 0,
    'rotor_item' => 0,
    'basic' => 0,
    'story' => 0,
    'topic' => 0,
    'university' => 0,
    'webform' => 0,
    'webpage' => 0,
    'workexperience' => 0,
    'work_experience_field_group' => 0,
    'workingpapers' => 0,
    'slideshow' => 0,
  );
  $export['newsfeed_content_type'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'newsfeed_pager_count';
  $strongarm->value = '10';
  $export['newsfeed_pager_count'] = $strongarm;

  return $export;
}
