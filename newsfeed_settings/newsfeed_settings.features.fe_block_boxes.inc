<?php

/**
 * Implementation of hook_default_fe_block_boxes().
 */
function newsfeed_settings_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass;
  $fe_block_boxes->info = 'My Disciplines';
  $fe_block_boxes->format = '7';
  $fe_block_boxes->machine_name = 'my_disciplines';
  $fe_block_boxes->body = '<?php
global $user;
if ($user->uid == 0) return;
$node_profile_contents = content_profile_load(\'basic\', $user->uid, $lang = \'\', $reset = null);
	$i = 0;
  $j = 0;
  if (count($node_profile_contents->taxonomy) > 0) {
    foreach ($node_profile_contents->taxonomy as $key => $values) {
      if ($values->vid == 2) {
        $termdis[$i] = $values->tid;
        $i++;
      }
      else {
        $termsgeo[$j] = $values->tid;
        $j++;
      }
    }
  }
  $dis = discipline_get_lists($termdis, 2);
	$dis_term = \'<ul>\';
  foreach ($dis as $key => $values) {
    $dis_term .= \'<li style="border-bottom: medium none; color:black; line-height: 18px; padding-left: 0px;">\';
    foreach ($values as $key1 => $val) {
      $dis_term .= l($val[\'label\'], \'taxonomy/term/\' . $val[\'value\']);
      if (count($values) - 1 != $key1)
        $dis_term .= \':&nbsp;\';
    }
    $dis_term .= \'</li>\';
  }
	$dis_term .= \'</ul>\';
print($dis_term);';

  $export['my_disciplines'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass;
  $fe_block_boxes->info = 'My Room';
  $fe_block_boxes->format = '7';
  $fe_block_boxes->machine_name = 'my_room';
  $fe_block_boxes->body = '<?php
  global $user;
  if($user->uid == 0) return;
  $result = db_query("SELECT COUNT(*) as c FROM user_relationships UR LEFT JOIN users U on UR.requestee_id = U.uid WHERE (UR.approved = \'1\') AND (UR.requester_id = \'%d\') ", $user->uid);
  $row = db_fetch_array($result);
  $following = $row[\'c\'];
  $result = db_query("SELECT COUNT(*) as c FROM user_relationships UR LEFT JOIN users U on UR.requestee_id = U.uid WHERE (UR.approved = \'1\') AND (UR.requestee_id = \'%d\') ", $user->uid);
  $row = db_fetch_array($result);
  $follower = $row[\'c\'];
  ?>
  <div class=\'myroom\'>
	<div class="header">
        </div>
        <div class="items">
		<div class="item first"><?php print(l(\'View My Profile\', "user/{$user->uid}"))?></div>
        	<div class="item"><div class="number"><?php print($following) ?></div><div class="text">Following</div></div>
		<div class="item last"><div class="number"><?php print($follower) ?></div><div class="text">Follower</div></div>
        </div>
  </div> 
 ';

  $export['my_room'] = $fe_block_boxes;

  return $export;
}
