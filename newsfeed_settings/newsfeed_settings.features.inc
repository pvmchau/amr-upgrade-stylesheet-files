<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function newsfeed_settings_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function newsfeed_settings_imagecache_default_presets() {
  $items = array(
    'newsfeed_author_image' => array(
      'presetname' => 'newsfeed_author_image',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '58',
            'height' => '58',
          ),
        ),
      ),
    ),
    'newsfeed_default' => array(
      'presetname' => 'newsfeed_default',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '58',
            'height' => '79',
          ),
        ),
      ),
    ),
    'newsfeed_group_thumbnail' => array(
      'presetname' => 'newsfeed_group_thumbnail',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '85',
            'height' => '69',
          ),
        ),
      ),
    ),
    'newsfeed_image_preview' => array(
      'presetname' => 'newsfeed_image_preview',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '250',
            'height' => '200',
          ),
        ),
      ),
    ),
    'newsfeed_video_preview' => array(
      'presetname' => 'newsfeed_video_preview',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '250',
            'height' => '200',
          ),
        ),
      ),
    ),
    'newsfeeds_comment_avatar' => array(
      'presetname' => 'newsfeeds_comment_avatar',
      'actions' => array(
        '0' => array(
          'weight' => '-10',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '35',
            'height' => '35',
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function newsfeed_settings_views_api() {
  return array(
    'api' => '2',
  );
}
