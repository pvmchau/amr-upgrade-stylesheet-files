$(document).ready(function(){
  $('#profile_right_block').height($('.profile_top').height() + $('#quicktabs-user_profile .quicktabs_tabs').height() + 9);
  val = $('#profile_right_block').height() - 50;
  $('#profile_right_block .inside').css('padding-top', val + "px");
  
    $('#slideshow') 
    .after('<div id="slideshow_nav">') 
    .cycle({ 
        fx: 'fade',
        pager: '#slideshow_nav', 
        next: '#slideshow_next', 
        prev: '#slideshow_prev'
    });
    
  //newsfeed js
    $('.media-embed .emvideo').hide();
    $('.media-embed').children('img').click(function () {
      $(this).hide();
      $(this).siblings('.play_button').hide();
      $(this).parent('.media-embed').css('height','360px');
      $(this).siblings('.emvideo').show();
    });
    $('.media-embed').children('.play_button').click(function () {
      $(this).hide();
      $(this).siblings('img').hide();
      $(this).parent('.media-embed').css('height','360px');
      $(this).siblings('.emvideo').show();
    });
    $('.homePopular .frame').each(function (i) {
        if($(this).children('div').hasClass('book-thumbnail')) {
            $(this).children('div.Book_author_image').hide();
            $(this).children('div.Article_author_image').hide();
            $(this).children('div._author_image').hide();
            $(this).children('div.Chapter_author_image').hide();
            $(this).children('div.Syllabus_author_image').hide();
        }
        if($(this).children('div').hasClass('Images_author_image')) {
            $(this).children('div.Images_author_image').siblings('.book-thumbnail').hide();
        }
        if($(this).children('div').hasClass('Groups_author_image')) {
            $(this).children('div.Groups_author_image').siblings('.book-thumbnail').hide();
        }
        if($(this).children('div').hasClass('Discussions_author_image')) {
            $(this).children('div.Discussions_author_image').siblings('.book-thumbnail').hide();
        }
        if($(this).children('div').hasClass('Blog_author_image')) {
            $(this).children('div.Blog_author_image').siblings('.book-thumbnail').hide();
        }
        if($(this).children('div').hasClass('Post_author_image')) {
            $(this).children('div.Post_author_image').siblings('.book-thumbnail').hide();
        }
        if($(this).children('div').hasClass('Lectures_author_image')) {
            $(this).children('div.Lectures_author_image').siblings('.book-thumbnail').hide();
        }
        if($(this).children('div').hasClass('Video_author_image')) {
            $(this).children('div.Video_author_image').siblings('.book-thumbnail').hide();
        }
    });
    $('.other .term').hide();
    $('.other .tooltip-icon').hide();
    $('.other').hover(
        function () {
          $(this).children('.term').fadeIn('slow');
          $(this).children('.tooltip-icon').fadeIn('slow');
        },
        function () {
          $(this).children('.term').fadeOut('slow');
          $(this).children('.tooltip-icon').fadeOut('slow');
        }
    );
});